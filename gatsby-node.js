const path = require(`path`);
// const { fmImagesToRelative } = require('gatsby-remark-relative-images');

// exports.onCreateNode = ({ node }) => {
//   fmImagesToRelative(node) // convert image paths for gatsby images
// }

exports.createPages = async ({ actions, graphql, reporter }) => {
  const { createPage } = actions;

  const blogPostTemplate = path.resolve(`src/templates/blog.js`);

  const result = await graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
      ) {
        edges {
          node {
            frontmatter {
              path
            }
          }
        }
      }
    }
  `);

  // Handle errors
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`);
    return
  }

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: node.frontmatter.path,
      component: blogPostTemplate,
      context: {}, // additional data can be passed via context
    })
  })
};

exports.onCreatePage = async ({ page, actions }) => {
  const { createPage } = actions;

  if (page.path.match(/^\/account/)) {
    page.matchPath = "/account/*";
    createPage(page)
  }
};

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  if (stage === 'build-html') {
    // Exclude Sign-In Widget from compilation path
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /okta-sign-in/,
            use: loaders.null(),
          }
        ],
      },
    })
  }
};

exports.createSchemaCustomization = ({ actions, schema }) => {
  const { createTypes } = actions
  const typeDefs = [
    schema.buildObjectType({
      name: "ContributorJson",
      fields: {
        name: "String!",
        firstName: "String!",
        email: "String!",
        receivedSwag: {
          type: "Boolean",
          resolve: source => source.receivedSwag || false,
        },
      },
      interfaces: ["Node"],
    }),
  ]
  createTypes(typeDefs)
}

// exports.createSchemaCustomization = ({ actions }) => {
//   const { createTypes } = actions
//   const typeDefs = `
//   type Query {
//     hello: String,
//     programs: [Program],
//     program(id: ID!): Program
//   }
//   type Program {
//     id: ID!,
//     title: String,
//     workouts: [Workout]
//   }
//   type Workout {
//     id: ID!,
//     title: String,
//     program: Program
//   }
//   `
//   createTypes(typeDefs)
// }

// exports.createResolvers = ({ createResolvers }) => {
//   let programs = [{ id: 1, title: "Program 1" }, { id: 2, title: "Program 2" }, { id: 3, title: "Program 3" }];
//   let workouts = [{ id: 1, title: "Workout 1", programId: 1 }, { id: 2, title: "Workout 2", programId: 1 }, { id: 3, title: "Workout 3", programId: 1 }];
//   const resolvers = {
//     Query: {
//       hello: { resolve: () => 'Hello world!' },
//       programs: { resolve: () => programs },
//       program: { resolve: (parent, { id }) => programs.find(el => el.id == id) }  
//     },
//     Program: {
//       workouts: { resolve: (smt) => Object.values(workouts).filter(workout => workout.programId == smt.id) }
//     }
//   }
//   console.log("RESOLVERS");

//   createResolvers(resolvers)
// }